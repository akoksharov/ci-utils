module gitlab.com/akoksharov/ci-utils

go 1.13

require (
	github.com/aws/aws-sdk-go v1.23.8
	github.com/gruntwork-io/terratest v0.23.0
	github.com/hashicorp/hcl v1.0.0
)
