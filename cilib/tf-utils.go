package cilib

import (
	"io/ioutil"
	"path"
	"strings"

	"github.com/hashicorp/hcl"
)

// AccountInfo contains account id and region read from provider configuration
type AccountInfo struct {
	ID     string
	Region string
}

// GetAccountID looks for aws provider configuration in .tf files and returns
// first item from allowed_account_ids list. If not found nil is.
func GetAccountID(tfDirPath string) (*AccountInfo, error) {
	files, err := ioutil.ReadDir(tfDirPath)
	if err != nil {
		return nil, err
	}

	result := &AccountInfo{
		ID:     "",
		Region: "",
	}

	for _, file := range files {
		// Iterate through teraform files only
		if strings.ToLower(path.Ext(file.Name())) != ".tf" {
			continue
		}

		tfFileContent, err := ioutil.ReadFile(path.Join(tfDirPath, file.Name()))
		if err != nil {
			continue
		}

		// Parse tf file content and get root object which is a hash string -> interface{}
		var hclRaw interface{}
		err = hcl.Unmarshal(tfFileContent, &hclRaw)
		if err != nil {
			continue
		}
		hclMap := hclRaw.(map[string]interface{})

		// Element of interest is 'provider'.
		// The correcponding object is an array of hashes string -> interface{}
		hclProviders := hclMap["provider"]
		if hclProviders == nil {
			continue
		}

		// Look through all items in providers array for the one which has 'aws' key.
		// The correcponding object is an array of hashes string -> interface{}
		var awsProviderConfigs interface{} = nil
		for _, provider := range hclProviders.([]map[string]interface{}) {
			if providerConfigs, found := provider["aws"]; found == true {
				awsProviderConfigs = providerConfigs
				break
			}
		}

		if awsProviderConfigs == nil {
			continue
		}

		// Look through the all items in aws provider configuration for the one which has allowed_account_ids key
		// The corresponding object is of an array of inretface{}
		var allowedAccountIds interface{} = nil
		for _, awsProviderConfig := range awsProviderConfigs.([]map[string]interface{}) {
			if region, found := awsProviderConfig["region"]; found == true {
				result.Region = region.(string)
			}
			if allowedIds, found := awsProviderConfig["allowed_account_ids"]; found == true {
				allowedAccountIds = allowedIds
				break
			}
		}

		if allowedAccountIds == nil {
			continue
		}

		// Pick first item in allowed_account_ids and return it as a string.
		for _, accountID := range allowedAccountIds.([]interface{}) {
			result.ID = accountID.(string)
			return result, nil
		}
	}

	return nil, nil
}
