package cilib

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

const (
	tfProvider1 string = `
provider "aws" {
  region              = "eu-central-1"
  allowed_account_ids = ["111"]
}
`
	tfProvideу2 string = `
provider "aws" {
  region              = "eu-central-1"
}
`
	tfData string = `
data "aws_ami" "image" {
  owners      = ["amazon"]
  most_recent = true
}
`
)

func TestGetAccountID_Defined(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider1.tf"), []byte(tfProvider1), 0644) != nil {
		t.Fatalf("Cant create provider1.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err != nil {
		t.Fatalf("Failed to read account_id '%v'", err)
	}
	if accountInfo.ID != "111" && accountInfo.Region != "eu-central-1" {
		t.Fatalf("Unexpected result")
	}
}

func TestGetAccountID_Empty(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider2.tf"), []byte(tfProvideу2), 0644) != nil {
		t.Fatalf("Cant create provider2.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err != nil {
		t.Fatalf("Failed to read account_id '%v'", err)
	}
	if accountInfo.ID != "" && accountInfo.Region != "eu-central-1" {
		t.Fatalf("Failed to read account_id")
	}
}

func TestGetAccountID_NotTfFile(t *testing.T) {

	testDir, err := ioutil.TempDir("/tmp", "ci-utils")
	if err != nil {
		t.Fatalf("Cant create temp directory '%v'", err)
	}
	defer os.RemoveAll(testDir)

	if ioutil.WriteFile(path.Join(testDir, "provider1.txt"), []byte(tfProvider1), 0644) != nil {
		t.Fatalf("Cant create provider1.tf '%v'", err)
	}
	if ioutil.WriteFile(path.Join(testDir, "data.tf"), []byte(tfData), 0644) != nil {
		t.Fatalf("Cant create data.tf '%v'", err)
	}

	accountInfo, err := GetAccountID(testDir)
	if err != nil {
		t.Fatalf("Failed to read account_id '%v'", err)
	}
	if accountInfo.ID != "" && accountInfo.Region != "" {
		t.Fatalf("Failed to read account_id")
	}
}
